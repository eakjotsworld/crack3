#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be
const int HASH_LEN=33;


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char pass[PASS_LEN];
    char hash[HASH_LEN];
};

int searchByHash(const void *t, const void *elem) {
    char *tt = (char *)t;
	struct entry *eelem = (struct entry *)elem;
	
	return strcmp(t, eelem->hash);
}

int cmp(const void *a, const void *b)
{
        struct entry *aa = (struct entry *)a;
        struct entry *bb = (struct entry *)b;
        
        return strcmp((*aa).hash, (*bb).hash);
}




// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	struct entry* entries = malloc(3000* sizeof(*entries));
	
	int counter = 0;
	char *pass = (char *)malloc(PASS_LEN * sizeof(char*));
	
    while(fgets(pass, PASS_LEN, in)) {
        struct entry passwordHash;
    	char *nl = strchr(pass, '\n');
		if (nl) *nl = '\0';
		
		
		char *hash = md5(pass, strlen(pass));
        strcpy(passwordHash.pass, pass);
        strcpy(passwordHash.hash, hash);
        
        entries[counter] = passwordHash;
        
        counter++;
    }
    
    *size = counter;
    return entries;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int size;
    struct entry *dict = read_dictionary(argv[2], &size);
    
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, size, sizeof(struct entry), cmp);
    
    
    
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".
    //struct entry *found = bsearch("f806fc5a2a0d5ba2471600758452799c", dict, 0, 0, NULL);

    // TODO
    // Open the hash file for reading.
    FILE *fp;
    
    char *line;
    line = (char *)malloc(HASH_LEN * sizeof(char*));
        
    if ((fp = fopen(argv[1], "r")) == NULL) {
        printf("Error! opening file");
        exit(1);
    }

    fp = fopen(argv[1], "r");
    while(fgets(line, HASH_LEN+1, fp)) {
        char *nl = strchr(line, '\n');
		if (nl) *nl = '\0';
		
		struct entry *found = bsearch(line, dict, size, sizeof(struct entry), searchByHash);
        if (found)
    	{
    		printf("%s %s\n", found->hash, found->pass);
    	}
    	else
    	{
    		printf("Not found.\n");
    	}
    	
    	
    }
	

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
}
